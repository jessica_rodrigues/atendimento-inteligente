import MySQLdb

#from __future__ import unicode_literals
from django.db import models


class Record(models.Model):
    name = models.CharField(max_length=5000)
    
    class Meta:
        verbose_name = u'Record'
        verbose_name_plural = u'Records'

    def getConnection(self):
        #return MySQLdb.connect(host='localhost', user='root', passwd='123',db='lideranca')
        return MySQLdb.connect(host='localhost', user='root', passwd='123',db='mktzap_master_meio_fevereiro')

    def getknowledgeBases(self):
        cursor = self.getConnection().cursor()
        cursor.execute("SELECT title, text FROM knowledge_bases_suporte_full")
        return cursor

    def getFullTextSearch(self, sentence):
        cursor = self.getConnection().cursor()

        #print sentence
        query = '''
                SELECT text, MATCH (text) AGAINST
                ("{sentence}" IN NATURAL LANGUAGE MODE) AS score
                FROM knowledge_bases_suporte
                WHERE MATCH (text) AGAINST ("{sentence}" IN NATURAL LANGUAGE MODE)
                LIMIT 5
                '''.format(sentence=sentence)

        #print query
        cursor.execute(query)
        return cursor
        
    def __str__(self):
        return self.name